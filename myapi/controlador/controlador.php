<?php

require_once './utilidades/entidades.php';
require_once './metodos/funciones.php';
require_once './metodos/producto.php';
require_once './metodos/login.php';

function GetDatosControaldor($datos) {

    $resp = Respuesta();

    $metodo = $datos['recurso'];

    //Lista de metodos
    switch ($metodo) {
        case 'producto':
            $resp = Producto($datos);
            break;
        case 'login':
            $resp = Login($datos);
            break;
        case 'nnnnnn':
        //
        default:
            $resp->estado = false;
            $resp->mensaje = "No existe recurso: " . $metodo;
            break;
    }

    return $resp;
}
