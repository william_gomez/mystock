<?php

ini_set('default_charset', 'utf-8');
header('Content-Type: application/json');
require_once './utilidades/entidades.php';
require_once './utilidades/funcionesglobales.php';
require_once './controlador/controlador.php';

        $respuesta = Respuesta(); 

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $postbody = file_get_contents("php://input");
            $datos = json_decode($postbody, true);

            if (json_last_error() == 0) {
                $respuesta = GetDatosControaldor($datos);               
                $jsonrequest = json_encode($respuesta);                           
                echo($jsonrequest);
                http_response_code(200);
            } else {
                $respuesta->estado=false;
                $respuesta->mensaje="Error decodificando json:".obtenerErrorDeJSON();
                //print_r("Error decodificando json: " . obtenerErrorDeJSON());
                $jsonrequest = json_encode($respuesta);                           
                echo($jsonrequest);
                http_response_code(400);
            }
        } else {
            $respuesta->estado=false;
            $respuesta->mensaje="Método incorrecto";
            $jsonrequest = json_encode($respuesta);                           
            echo($jsonrequest);
            http_response_code(405);
        }
    