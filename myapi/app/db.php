<?php

require_once './utilidades/entidades.php';
ini_set('default_charset', 'utf-8');
//mb_internal_encoding('UTF-8');
//mb_http_output('UTF-8');

$user = "root";
$dbname = "my_stock_db";
$password = null;
$port = "3306";
$host = "localhost";

//String de conexion
$conexion = mysqli_connect($host,$user,$password,$dbname ,$port);

$respDb = Respuesta();

try {
    
    if ($conexion->connect_errno) {
        $conexion=null;
        $respDb->respuesta="Error conectando a base de datos";  
    }else{
        $respDb->estado=true; 
    }

} catch (Exception $ex) {
    $conexion=null;
    $respDb->respuesta="Error conectando a base de datos";       
}


//Guardar, modificar, eliminar

function NonQuery($query){

    global $respDb;
    global $conexion;
     
    if($conexion){  

    $result = $conexion->query($query);

    $respDb->estado=true;
    $respDb->respuesta=$conexion->affected_rows;

    }

    return $respDb;
}


//Select

function ObtenerRegistros($query){
    
    global $respDb;
    global $conexion;
     
    try {

            $result = mysqli_query($conexion,$query);

            $resultArray = array();
    
            while($row =mysqli_fetch_assoc($result))
            {
                $resultArray[]  = $row;
            }

           $respDb->estado=true;
           $respDb->respuesta=$resultArray;
    
           mysqli_close($conexion);

    } catch (Exception $ex) {
        $conexion=null;
        $respDb->respuesta=$ex;       
    }
  
    return $respDb;
}


//uft8

function ConvertirUTF8($array){

    $array= array_map('encode_all_strings', $array);

    array_walk_recursive($array,function(&$item,$key){
        if(!mb_detect_encoding($item,'utf-8',true)){
        $item = utf8_encode($item);
        }
    });

    return $array;

}


function utf8_converter($array)
{
    array_walk_recursive($array, function(&$value, $key) {
        if (is_string($value)) {

            $value = iconv('windows-1252', 'utf-8', $value);
        }
    });

    return $array;
}