
var usuario = document.getElementById("usuario")
var clave = document.getElementById("clave")
var alert = document.getElementById("alert")

alert.style.color = 'red';

var form = document.getElementById('formulario_login');

form.addEventListener('submit', function(evt){
    evt.preventDefault();

    var mensajeError = [];
    var error;
    
    if(usuario.value === null || clave.value === ''){
        mensajeError.push("Debe ingresar el usuario");
        error =true;
    }
    
    if(clave.value === null || clave.value === ''){
        mensajeError.push("Debe ingresar la clave");
        error =true;
    }
       
    if(error == true){
        alert.innerHTML = mensajeError.join(', ')

    }else{
      
        //Enviamos datos de usuario a la api
  
        postDataLogin('http://34.205.129.19/myapi/', { recurso: "login", accion: "valida_usuario", datos:{ usuario: usuario.value, clave: clave.value } })
        .then(data => {
        console.log(data);
        });


        location.href = 'html/main.html';
    }


});


// Implementando el metodo POST:
async function postDataLogin(url = '', data = {}) {

    console.log(JSON.stringify(data));

    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });

    return response.json();
  }
